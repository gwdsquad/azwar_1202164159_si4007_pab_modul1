package com.example.android.modul1muhammadtubagus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

        private EditText Alasan, Tinggian;
        private Button Cekan;
        private TextView Hasilan;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Alasan = (EditText) findViewById(R.id.editalas);
            Tinggian = (EditText) findViewById(R.id.edittinggi);
            Cekan = (Button) findViewById(R.id.button_cek);
            Hasilan = (TextView) findViewById(R.id.texthasil);

            Cekan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Alasan1, Tinggian1;
                    Alasan1 = Alasan.getText().toString();
                    Tinggian1 = Tinggian.getText().toString();
                    if (TextUtils.isEmpty(Alasan1)){
                        Alasan.setError("Tidak Boleh Kosng ! !");
                        Alasan.requestFocus();

                    } else if (TextUtils.isEmpty(Tinggian1)){
                        Tinggian.setError("Tidak Boleh Kosong ! !");
                        Tinggian.requestFocus();

                    } else {
                        double a = Double.parseDouble(Alasan1);
                        double t = Double.parseDouble(Tinggian1);
                        double hasil = a * t;


                        Hasilan.setText(String.valueOf(hasil));

                    }
                }

            });
        }
    }